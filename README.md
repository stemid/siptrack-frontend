# Ansible role: siptrack-frontend

Role to setup the [siptrackweb](https://github.com/sii/siptrackweb) Django frontend.

# Dependencies

## Services

* [siptrackd](https://github.com/sii/siptrackd) backend API (perhaps deployed by the [siptrack-backend role](https://gitlab.com/stemid/siptrack-backend)).
* nginx - this role installs nginx config under ``/etc/nginx/conf.d``.

## System config

* siptrack system user for installing files and running services
* virtualenv directory owned by the siptrack user
* sudoers config allowing your ansible admin user to sudo as the siptrack user

# Role parameters

## Important parameters

* ``siptrack_environment`` - This must be set first from your playbook or inventory, determines installation path and service names for example.
* ``siptrackweb_secret`` - Django secret string that must be randomized for each environment, and preferably kept in a vault.

## Other parameters

Check the ''defaults/main.yml'' file for quick access to all the variables you can override.

* ``siptrack_username`` - User that will own all siptrack files, including virtualenv, and run the service.
* ``siptrack_home`` - Home dir of the user.
* ``siptrackd_host`` - Name or IP that siptrackd service listens to (port 9242 and 9243).
* ``siptrackweb_virtualenv`` - Path to virtualenv owned by siptrack user.
* ``siptrackweb_domain`` - The domain written to the web server config.
* ``siptrackweb_admin_email`` - A Django setting.
* ``siptrackweb_cmd`` - The default command to use in the systemd service Exec directive.
* ``siptrackweb_args`` - Additional args appended to the previous string.

# Examples

**Disclaimer:** These examples include roles I have locally, but they should be self-explanatory.

## Bootstrap

See the [Bootstrap section](https://gitlab.com/stemid/siptrack-backend#bootstrap) in the [siptrack-backend repo](https://gitlab.com/stemid/siptrack-backend).

## Install

```yaml
---

- hosts: frontend
  become: True

  vars_files:
    - vars/common.yml
    - "vars/{{ siptrack_environment }}.yml"

  roles:
    - role: nginx

    - role: siptrack-frontend
      siptrackweb_domain: siptrack.mylan.local
```
